 * remove InitIceOryxService & InitIceOryxSubscriber completely and replace it 
   with CreateService

 * Tester sends commands to all services 
 * Camera, StereoCamera, TrackingCamera are entering simulation mode if not found

> refactor ServiceInfoProvider (services must register there)

> fix exception throw in ServiceInfoProvider:
    terminate called after throwing an instance of 'std::experimental::filesystem::v1::__cxx11::filesystem_error'
      what():  filesystem error: status: No such process [/proc/1421461/comm]
    ./startAllServices.sh: line 6: 201584 Aborted                 (core dumped) build/ServiceInfoProvider


