#pragma once

#include <cstdint>

namespace serviceData
{
template < uint64_t Capacity >
struct rawMemory_t
{
    uint64_t size{ 0 };
    uint8_t  data[Capacity];
};
} // namespace serviceData
