#pragma once

#include "iceoryx_hoofs/cxx/string.hpp"
#include "iceoryx_hoofs/cxx/vector.hpp"

namespace serviceData
{
struct identifier_t
{
    using string = iox::cxx::string< 64 >;
    using vector = iox::cxx::vector< string, 64 >;

    string name;
    vector availableServices;
};
} // namespace serviceData
