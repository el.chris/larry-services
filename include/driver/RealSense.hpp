#pragma once

#ifdef ENABLE_LIBREALSENSE

#include "serviceData/stereoCamera_t.hpp"
#include "serviceData/trackingCamera_t.hpp"

#include <librealsense2/h/rs_option.h>
#include <librealsense2/h/rs_sensor.h>
#include <librealsense2/hpp/rs_frame.hpp>
#include <librealsense2/hpp/rs_processing.hpp>
#include <librealsense2/rs.hpp>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace driver
{
class RealSense
{
  public:
    struct device_t
    {
        device_t( const std::string&  sensorName,
                  const rs2::context& context ) noexcept;
        ~device_t();
        device_t( const device_t& ) = delete;
        device_t( device_t&& rhs ) noexcept;

        device_t&
        operator=( const device_t& ) = delete;
        device_t&
        operator=( device_t&& rhs ) noexcept;

        rs2::pipeline pipeline;
        rs2::config   config;
        bool          isInitialized{ false };

      private:
        void
        destroy() noexcept;
    };

    struct stereoCamera_t
    {
        bool
        GetOutput( serviceData::stereoCamera_t& newData );
        void
        StartStreaming();

        device_t device;

        struct filter_t
        {
            filter_t();
            void
            process( rs2::frame& frame ) const;

            rs2::decimation_filter   decimation;
            rs2::disparity_transform depthToDisparity{ true };
            rs2::disparity_transform disparityToDepth{ false };
            rs2::temporal_filter     temporal;
            rs2::hole_filling_filter holeFilling;
        } filter;
    };

    struct trackingCamera_t
    {
        bool
        GetOutput( serviceData::trackingCamera_t& newData );
        void
        StartStreaming();

        device_t device;
    };

    std::optional< stereoCamera_t >
    AcquireStereoCamera( const std::string& sensorName );
    std::optional< trackingCamera_t >
    AcquireTrackingCamera( const std::string& sensorName );

  private:
    std::unique_ptr< rs2::context > context{ new rs2::context };
};
} // namespace driver
#endif
