#pragma once

#ifdef ENABLE_GPIO
#include <wiringPi.h>
#endif

namespace gpio
{
class GPIOReader
{
  public:
    explicit GPIOReader( const int pin ) noexcept;
    bool
    IsHigh() const noexcept;

  private:
#ifdef ENABLE_GPIO
    int pin;
#endif
};
} // namespace gpio
