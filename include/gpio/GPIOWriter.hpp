#pragma once

#include "units/Time.hpp"

#ifdef ENABLE_GPIO
#include <wiringPi.h>
#endif

namespace gpio
{
class GPIOWriter
{
  public:
    explicit GPIOWriter( const int pin ) noexcept;

    void
    High( const el3D::units::Time delay =
              el3D::units::Time::Seconds( 0.0f ) ) noexcept;
    void
    Low( const el3D::units::Time delay =
             el3D::units::Time::Seconds( 0.0f ) ) noexcept;
    bool
    IsHigh() const noexcept;

  private:
    void
    Write( const int value, const el3D::units::Time delay ) noexcept;

  private:
#ifdef ENABLE_GPIO
    int pin;
#endif
    bool isHigh{ false };
};
} // namespace gpio
