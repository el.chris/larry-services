#include "iceoryx_posh/capro/service_description.hpp"
#include "initializer/initializer.hpp"
#include "serviceData/Driver.hpp"
#include "serviceData/ultraSonicSensor_t.hpp"
#include "units/Length.hpp"

#include <chrono>
#include <limits>

using namespace el3D::units;
using number_t = el3D::bb::float_t< 64 >;

int
main()
{
    using namespace iox;

    runtime::PoshRuntime::initRuntime( "Explorer" );


    auto ultraSonicSensorData =
        iox::popo::Subscriber< serviceData::ultraSonicSensor_t >(
            capro::ServiceDescription( "larry", "UltraSonicSensor",
                                       "intern" ) );
    ultraSonicSensorData.subscribe();

    serviceData::Driver driver;
    std::cout << "up and running\n";

    Length minimumDistance = Length::Meter( 0.3 );
    while ( true )
    {
        ultraSonicSensorData.take().and_then(
            [&]( iox::popo::Sample< const serviceData::ultraSonicSensor_t >&
                     sensorData )
            {
                if ( sensorData->distance < minimumDistance )
                {
                    driver.TurnRight();
                    std::this_thread::sleep_for( std::chrono::seconds( 1 ) );
                }
                else if ( sensorData->distance >= minimumDistance )
                {
                    driver.Forward();
                }
            } );
    }
}
