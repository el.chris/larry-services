#include "cui/Cui.hpp"
#include "cui/KeyStatusPair.hpp"
#include "cui/KeyValuePair.hpp"
#include "cui/Label.hpp"
#include "cui/StaticSymbol.hpp"
#include "cuipron/ConsoleControl.hpp"
#include "iceoryx_posh/capro/service_description.hpp"
#include "initializer/initializer.hpp"
#include "serviceData/camera_t.hpp"
#include "serviceData/drive_t.hpp"
#include "serviceData/ledControl_t.hpp"

#include <iostream>

struct vec2
{
    int64_t x;
    int64_t y;
};

class Larry
{
  public:
    Larry( cui::Cui* const cui, const int32_t minHeight,
           const vec2 position ) noexcept;

    void
    MoveForward();
    void
    MoveBackward();
    void
    RotateLeft();
    void
    RotateRight();

  private:
    void
    SetPosition( const vec2 newPosition ) noexcept;
    void
    UpdatePosition();

  private:
    cui::Cui*                            cui;
    vec2                                 position{ 0, 0 };
    vec2                                 lookAt{ 0, 1 };
    int64_t                              minHeight{ 0 };
    std::shared_ptr< cui::StaticSymbol > larryCenter;
    std::shared_ptr< cui::StaticSymbol > larryArrow;
    std::shared_ptr< cui::StaticSymbol > cleanup;
};

Larry::Larry( cui::Cui* const cui, const int32_t minHeight,
              const vec2 position ) noexcept
    : cui( cui ), position( position ), minHeight( minHeight )
{
    this->larryCenter = this->cui->Create< cui::StaticSymbol >(
        { .size = { 1, 1 },
          .position =
              cp::vec2{ static_cast< uint16_t >( this->position.x ),
                        static_cast< uint16_t >( this->position.y ) } } );
    this->larryCenter->SetContent( "O" );

    this->cleanup = this->cui->Create< cui::StaticSymbol >(
        { .size     = { 1, 1 },
          .position = {
              static_cast< uint16_t >( this->position.x + this->lookAt.x ),
              static_cast< uint16_t >( this->position.y +
                                       this->lookAt.y ) } } );
    this->cleanup->SetContent( " " );

    this->larryArrow = this->cui->Create< cui::StaticSymbol >(
        { .size     = { 1, 1 },
          .position = {
              static_cast< uint16_t >( this->position.x - this->lookAt.x ),
              static_cast< uint16_t >( this->position.y -
                                       this->lookAt.y ) } } );
    this->larryArrow->SetContent( "|" );
}

void
Larry::RotateLeft()
{
    this->cleanup->SetPosition(
        { static_cast< uint16_t >( this->position.x - this->lookAt.x ),
          static_cast< uint16_t >( this->position.y - this->lookAt.y ) } );

    if ( this->lookAt.x == 0 && this->lookAt.y == 1 )
    {
        this->larryArrow->SetContent( "-" );
        this->lookAt = { 1, 0 };
    }
    else if ( this->lookAt.x == 1 && this->lookAt.y == 0 )
    {
        this->larryArrow->SetContent( "|" );
        this->lookAt = { 0, -1 };
    }
    else if ( this->lookAt.x == 0 && this->lookAt.y == -1 )
    {
        this->larryArrow->SetContent( "-" );
        this->lookAt = { -1, 0 };
    }
    else if ( this->lookAt.x == -1 && this->lookAt.y == 0 )
    {
        this->larryArrow->SetContent( "|" );
        this->lookAt = { 0, 1 };
    }

    this->UpdatePosition();
}

void
Larry::RotateRight()
{
    this->cleanup->SetPosition(
        { static_cast< uint16_t >( this->position.x - this->lookAt.x ),
          static_cast< uint16_t >( this->position.y - this->lookAt.y ) } );

    if ( this->lookAt.x == 0 && this->lookAt.y == 1 )
    {
        this->larryArrow->SetContent( "-" );
        this->lookAt = { -1, 0 };
    }
    else if ( this->lookAt.x == 1 && this->lookAt.y == 0 )
    {
        this->larryArrow->SetContent( "|" );
        this->lookAt = { 0, 1 };
    }
    else if ( this->lookAt.x == 0 && this->lookAt.y == -1 )
    {
        this->larryArrow->SetContent( "-" );
        this->lookAt = { 1, 0 };
    }
    else if ( this->lookAt.x == -1 && this->lookAt.y == 0 )
    {
        this->larryArrow->SetContent( "|" );
        this->lookAt = { 0, -1 };
    }

    this->UpdatePosition();
}

void
Larry::MoveForward()
{
    this->SetPosition(
        { static_cast< uint16_t >( this->position.x - this->lookAt.x ),
          static_cast< uint16_t >( this->position.y - this->lookAt.y ) } );
    this->cleanup->SetPosition(
        { static_cast< uint16_t >( this->position.x + this->lookAt.x ),
          static_cast< uint16_t >( this->position.y + this->lookAt.y ) } );
    this->UpdatePosition();
}

void
Larry::MoveBackward()
{
    this->SetPosition(
        { static_cast< uint16_t >( this->position.x + this->lookAt.x ),
          static_cast< uint16_t >( this->position.y + this->lookAt.y ) } );
    this->cleanup->SetPosition(
        { static_cast< uint16_t >( this->position.x - 2 * this->lookAt.x ),
          static_cast< uint16_t >( this->position.y - 2 * this->lookAt.y ) } );
    this->UpdatePosition();
}

void
Larry::SetPosition( const vec2 newPosition ) noexcept
{
    this->position = {
        std::clamp( newPosition.x, static_cast< int64_t >( 1 ),
                    static_cast< int64_t >(
                        cp::ConsoleControl().GetTerminalDimensions().x - 2 ) ),
        std::clamp(
            newPosition.y, this->minHeight,
            static_cast< int64_t >(
                cp::ConsoleControl().GetTerminalDimensions().y - 2 ) ) };
}


void
Larry::UpdatePosition()
{
    this->larryArrow->SetPosition(
        { static_cast< uint16_t >( this->position.x - this->lookAt.x ),
          static_cast< uint16_t >( this->position.y - this->lookAt.y ) } );
    this->larryCenter->SetPosition(
        cp::vec2{ static_cast< uint16_t >( this->position.x ),
                  static_cast< uint16_t >( this->position.y ) } );
}

int
main()
{
    iox::runtime::PoshRuntime::initRuntime( "Tester" );
    auto drive =
        initializer::CreatePort< iox::popo::Publisher< serviceData::drive_t > >(
            "Drive", initializer::ServiceType::Command,
            initializer::GetIceOryxConfig() );

    auto driveInfo = initializer::CreatePort<
        iox::popo::Subscriber< serviceData::drive_t > >(
        "Drive", initializer::ServiceType::Info,
        initializer::GetIceOryxConfig() );

    auto led = initializer::CreatePort<
        iox::popo::Publisher< serviceData::ledControl_t > >(
        "LedControl", initializer::ServiceType::Command,
        initializer::GetIceOryxConfig() );

    auto ledInfo = initializer::CreatePort<
        iox::popo::Subscriber< serviceData::ledControl_t > >(
        "LedControl", initializer::ServiceType::Info,
        initializer::GetIceOryxConfig() );

    auto camCmd = initializer::CreatePort<
        iox::popo::Publisher< serviceData::cameraCommand_t > >(
        "Camera", initializer::ServiceType::Command,
        initializer::GetIceOryxConfig() );

    auto camInfo = initializer::CreatePort<
        iox::popo::Subscriber< serviceData::camera_t<> > >(
        "Camera", initializer::ServiceType::Info,
        initializer::GetIceOryxConfig() );

    drive.offer();
    driveInfo.subscribe();
    led.offer();
    ledInfo.subscribe();
    camCmd.offer();
    camInfo.subscribe();

    cp::ConsoleControl().ClearTerminal();
    cui::Cui cui( 1 );
    Larry    larry( &cui, 5, { 10, 10 } );

    auto width = cp::ConsoleControl().GetTerminalDimensions().x;

    auto keyDriveInfo = cui.Create< cui::KeyValuePair >(
        { .size = { width, 1 }, .position = { 1, 0 } } );
    keyDriveInfo->SetTitle( [] { return "drive keys"; } );
    keyDriveInfo->SetValue( [] { return "'w', 'a', 's', 'd'"; } );

    auto keyQuitInfo = cui.Create< cui::KeyValuePair >(
        { .size = { width, 1 }, .position = { 1, 1 } } );
    keyQuitInfo->SetTitle( [] { return "quit"; } );
    keyQuitInfo->SetValue( [] { return "ESC"; } );

    auto keyCameraInfo = cui.Create< cui::KeyValuePair >(
        { .size = { width, 1 }, .position = { 40, 0 } } );
    keyCameraInfo->SetTitle( [] { return "camera keys"; } );
    keyCameraInfo->SetValue( [] { return "'q', 'e', 'r', 'f'"; } );

    auto keyLedInfo = cui.Create< cui::KeyValuePair >(
        { .size = { width, 1 }, .position = { 40, 1 } } );
    keyLedInfo->SetTitle( [] { return "led control keys"; } );
    keyLedInfo->SetValue( [] { return "'z', 'x', 'c'"; } );

    auto commandStatus = cui.Create< cui::KeyStatusPair >(
        { .size = { width, 1 }, .position = { 1, 2 } } );
    commandStatus->SetTitle( [] { return "command acknowledgement:"; } );

    std::atomic_uint16_t oldCamAngleX{ 0 }, oldCamAngleY{ 0 };
    commandStatus->SetValue(
        [&]() -> std::string
        {
            static std::string lastReturn = "";
            driveInfo.take().and_then(
                [&]( iox::popo::Sample< const serviceData::drive_t >&
                         driveSample )
                {
                    if ( driveSample->left == 100 && driveSample->right == 100 )
                        larry.MoveForward();
                    else if ( driveSample->left == -100 &&
                              driveSample->right == -100 )
                        larry.MoveBackward();
                    else if ( driveSample->left == -100 &&
                              driveSample->right == 100 )
                        larry.RotateLeft();
                    else if ( driveSample->left == 100 &&
                              driveSample->right == -100 )
                        larry.RotateRight();

                    lastReturn =
                        "drive: " + std::to_string( driveSample->left ) + ":" +
                        std::to_string( driveSample->right );
                } );

            ledInfo.take().and_then(
                [&]( iox::popo::Sample< const serviceData::ledControl_t >&
                         ledSample )
                {
                    lastReturn = "led: R=" + std::to_string( ledSample->red ) +
                                 " G=" + std::to_string( ledSample->green ) +
                                 " B=" + std::to_string( ledSample->blue );
                } );

            camInfo.take().and_then(
                [&]( iox::popo::Sample< const serviceData::camera_t<> >&
                         camSample )
                {
                    if ( ( oldCamAngleY != camSample->angle.yAngle ||
                           oldCamAngleX != camSample->angle.xAngle ) )
                    {
                        oldCamAngleX = camSample->angle.xAngle;
                        oldCamAngleY = camSample->angle.yAngle;

                        lastReturn = "camera angle update [ " +
                                     std::to_string( oldCamAngleX ) + ", " +
                                     std::to_string( oldCamAngleY ) + " ]";
                    }
                } );

            return lastReturn;
        } );

    auto sensorStatus = cui.Create< cui::KeyStatusPair >(
        { .size = { width, 1 }, .position = { 1, 3 } } );
    sensorStatus->SetTitle( [] { return "sensor status:"; } );
    sensorStatus->SetValue( [] { return ""; } );


    serviceData::ledControl_t ledState;
    while ( true )
    {
        system( "stty raw" );
        system( "stty -echo" );
        int c = std::cin.get();
        system( "stty cooked" );
        system( "stty echo" );

        if ( c == 27 ) break; // ESC pressed
        // camera horizontal
        else if ( c == 'q' || c == 'e' || c == 'r' || c == 'f' )
        {
            serviceData::cameraCommand_t command;
            command.xAngle = oldCamAngleX;
            command.yAngle = oldCamAngleY;

            if ( c == 'e' )
                command.xAngle++;
            else if ( c == 'q' )
                command.xAngle--;
            else if ( c == 'r' )
                command.yAngle++;
            else if ( c == 'f' )
                command.yAngle--;

            camCmd.publishCopyOf( command );
        } // drive
        else if ( c == 'w' || c == 's' || c == 'a' || c == 'd' )
        {
            serviceData::drive_t command;
            if ( c == 'w' )
                command = serviceData::drive_t{ 100, 100 };
            else if ( c == 's' )
                command = serviceData::drive_t{ -100, -100 };
            else if ( c == 'a' )
                command = serviceData::drive_t{ -100, 100 };
            else if ( c == 'd' )
                command = serviceData::drive_t{ 100, -100 };

            drive.publishCopyOf( command );
        }
        // led
        else if ( c == 'z' || c == 'x' || c == 'c' )
        {
            serviceData::ledControl_t command = ledState;
            if ( c == 'z' )
                command.red = !command.red;
            else if ( c == 'x' )
                command.green = !command.green;
            else if ( c == 'c' )
                command.blue = !command.blue;

            led.publishCopyOf( command );
            ledState = command;
        }
    }
}
