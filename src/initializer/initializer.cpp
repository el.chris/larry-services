#include "initializer/initializer.hpp"

#include "logging/policies/format.hpp"
#include "lua/elConfigHandler.hpp"

#include <exception>
#include <filesystem>
#include <memory>

namespace initializer
{
void
InitializeLoggingToConsole()
{
    using namespace el3D;
    static logging::output::ToTerminal< logging::threading::ThreadSafe >
        consoleLog;

    consoleLog.SetFormatting( logging::format::Minimal );
    consoleLog.SetLogLevel( logging::DEBUG );

    logging::elLog::AddOutput( 0, &consoleLog );
    logging::elLog::EnableOutputRedirection();
}

void
InitializeLoggingToLogfile( const std::string& logfile )
{
    using namespace el3D;
    static logging::output::ToFile< logging::threading::InSeparateThread >
        fileLog( logfile );

    fileLog.SetFormatting( logging::format::Detailled );
    fileLog.SetLogLevel( logging::DEBUG );

    logging::elLog::AddOutput( 0, &fileLog );
}

el3D::lua::elConfigHandler&
GetConfig()
{
    static el3D::lua::elConfigHandler config;
    static bool                       firstRun = true;

    if ( firstRun && config.SetConfig( initializer::CONFIG_DIR ).HasError() )
    {
        LOG_FATAL( 0 ) << "unable to load config in \""
                       << initializer::CONFIG_DIR << "\"";
        exit( 1 );
    }

    firstRun = false;
    return config;
}

iox::capro::ServiceDescription
CreateServiceDescription( const std::string& instance, const ServiceType& type,
                          const iceOryxConfig_t& config )
{
    using namespace iox::capro;
    using namespace iox::cxx;

    std::string eventName, serviceName = config.domain;
    if ( type == ServiceType::Announce )
    {
        eventName = config.eventAnnounce;
        serviceName += ENTRY_SEPARATOR + config.eventAnnounce;
    }
    else if ( type == ServiceType::Command )
    {
        eventName = config.entityName + ENTRY_SEPARATOR + config.eventCommand;
    }
    else if ( type == ServiceType::Info )
    {
        eventName = config.entityName + ENTRY_SEPARATOR + config.eventInfo;
    }
    else if ( type == ServiceType::Simulate )
    {
        eventName = config.entityName + ENTRY_SEPARATOR + config.eventSimulate;
    }

    return ServiceDescription{ IdString_t( TruncateToCapacity, serviceName ),
                               IdString_t( TruncateToCapacity, instance ),
                               IdString_t( TruncateToCapacity, eventName ) };
}


iceOryxConfig_t
GetIceOryxConfig()
{
    auto robotDomain = GetConfigEntry< std::string >(
        []( auto& ) { return true; }, { "Robot", "domain" },
        "Robot.domain is missing." );

    auto robotName = GetConfigEntry< std::string >(
        []( auto& ) { return true; }, { "Robot", "name" },
        "Robot.name is missing." );

    auto eventCommandID = GetConfigEntry< std::string >(
        []( auto& ) { return true; }, { "IceOryx", "eventCommand" },
        "IceOryx.eventCommand is missing." );

    auto eventInfoID = GetConfigEntry< std::string >(
        []( auto& ) { return true; }, { "IceOryx", "eventInfo" },
        "IceOryx.eventInfo is missing." );

    auto eventSimulationID = GetConfigEntry< std::string >(
        []( auto& ) { return true; }, { "IceOryx", "eventSimulation" },
        "IceOryx.eventSimulation is missing." );

    auto eventAnnounceID = GetConfigEntry< std::string >(
        []( auto& ) { return true; }, { "IceOryx", "eventAnnounce" },
        "IceOryx.eventAnnounce is missing." );

    return iceOryxConfig_t{ robotDomain[0],       robotName[0],
                            eventCommandID[0],    eventInfoID[0],
                            eventSimulationID[0], eventAnnounceID[0] };
}

serviceConfig_t
GetServiceConfig( const std::string& serviceName )
{
    serviceConfig_t serviceConfig;

    auto version =
        GetConfigEntry< int >( []( auto& v ) { return !v.empty(); },
                               { "services", serviceName, "version" },
                               "Service version entry is missing." );
    serviceConfig.version = static_cast< uint32_t >( version[0] );

    auto port = GetConfigEntry< int >( []( auto& v ) { return !v.empty(); },
                                       { "services", serviceName, "port" },
                                       "Service port entry is missing." );
    serviceConfig.port    = static_cast< uint16_t >( port[0] );
    serviceConfig.iceoryx = GetIceOryxConfig();

    auto logfile = GetConfigEntry< std::string >(
        []( auto& v ) { return !v.empty() && !v[0].empty(); },
        { "services", serviceName, "logfile" },
        "Service logfile entry is missing." );
    serviceConfig.logfile = logfile[0];

    auto logDirectory = GetConfigEntry< std::string >(
        []( auto& v ) { return !v.empty() && !v[0].empty(); },
        { "services", "logDirectory" }, "logDirectory entry is missing." );
    serviceConfig.logDirectory = logDirectory[0];

    return serviceConfig;
}

serviceConfig_t
InitGenericService( const std::string& serviceName )
{
    using namespace el3D;
    namespace fs = std::filesystem;

    InitializeLoggingToConsole();

    auto service = GetServiceConfig( serviceName );

    fs::path logDir( service.logDirectory );
    if ( fs::exists( logDir ) && !fs::is_directory( logDir ) )
    {
        std::cerr << "log directory \"" << service.logDirectory
                  << "\" exists and is not a directory\n";
        std::terminate();
    }
    else if ( !fs::exists( logDir ) )
    {
        fs::create_directory( logDir );
    }

    InitializeLoggingToLogfile( service.logDirectory + "/" + service.logfile );
    return service;
}


std::unique_ptr< el3D::network::elEasyTCPMessaging_Server >
InitService( const std::string& serviceName )
{
    using namespace el3D;
    auto service = InitGenericService( serviceName );
    std::unique_ptr< network::elEasyTCPMessaging_Server > server =
        std::make_unique< network::elEasyTCPMessaging_Server >(
            serviceName, service.version );
    if ( server->Listen( service.port ).HasError() ) return nullptr;

    return server;
}

} // namespace initializer
