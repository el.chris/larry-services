#include "gpio/init.hpp"

namespace gpio
{
void
init() noexcept
{
    static bool isInitialized = false;

    if ( !isInitialized )
    {
#ifdef ENABLE_GPIO
        wiringPiSetup();
#endif
    }
}
} // namespace gpio
