#include "serviceData/Driver.hpp"

#include <thread>

namespace serviceData
{
Driver::Driver()
    : publisher( iox::capro::ServiceDescription( "larry", "Drive", "extern" ) )
{
    publisher.offer();
}

void
Driver::PublishDrivingCommand( const int8_t left, const int8_t right ) noexcept
{
    if ( left == this->lastLeft && right == this->lastRight ) return;

    this->publisher.publishCopyOf( { left, right } );
    this->lastLeft  = left;
    this->lastRight = right;
}

void
Driver::TurnLeft() noexcept
{
    this->PublishDrivingCommand( this->speedReverse, this->speed );

    std::this_thread::sleep_for( std::chrono::milliseconds( 1500 ) );
    this->Stop();
}

void
Driver::TurnRight() noexcept
{
    this->PublishDrivingCommand( this->speed, this->speedReverse );

    std::this_thread::sleep_for( std::chrono::milliseconds( 1500 ) );
    this->Stop();
}

void
Driver::Forward() noexcept
{
    this->PublishDrivingCommand( this->speed, this->speed );
}

void
Driver::Backward() noexcept
{
    this->PublishDrivingCommand( this->speedReverse, this->speedReverse );
}

void
Driver::Stop() noexcept
{
    this->PublishDrivingCommand( 0, 0 );
}

} // namespace serviceData
