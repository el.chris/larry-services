#include "driver/MotorController.hpp"
#include "initializer/initializer.hpp"
#include "network/elEasyTCPMessaging_Server.hpp"
#include "network/etm/Transmission_t.hpp"
#include "network/etm/etm.hpp"
#include "serviceData/camera_t.hpp"
#include "serviceData/rawMemory_t.hpp"
#include "utils/elWebcam.hpp"
#include "utils/memcpy_pp.hpp"

#include <chrono>
#include <cstdint>
#include <iostream>

using namespace el3D;

constexpr char SERVICE[] = "Camera";

std::vector< int >         resolution;
std::vector< std::string > device;

int verticalPin, verticalBias, horizontalPin, horizontalBias;

void
LoadCustomConfig()
{
    resolution = initializer::GetConfigEntry< int >(
        []( auto& v ) { return v.size() == 2; },
        { "services", SERVICE, "resolution" }, "Invalid camera resolution!" );

    device = initializer::GetConfigEntry< std::string >(
        []( auto& v ) { return !v.empty() && !v[0].empty(); },
        { "services", SERVICE, "device" }, "Invalid camera device!" );

    horizontalPin = initializer::GetConfigEntry< int >(
        []( auto& v ) { return !v.empty(); },
        { "services", SERVICE, "horizontalPin" },
        "Invalid horizontal motor pin!" )[0];

    horizontalBias = initializer::GetConfigEntry< int >(
        []( auto& v ) { return !v.empty(); },
        { "services", SERVICE, "horizontalBias" },
        "Invalid horizontal motor bias!" )[0];

    verticalPin = initializer::GetConfigEntry< int >(
        []( auto& v ) { return !v.empty(); },
        { "services", SERVICE, "verticalPin" },
        "Invalid vertical motor pin!" )[0];

    verticalBias = initializer::GetConfigEntry< int >(
        []( auto& v ) { return !v.empty(); },
        { "services", SERVICE, "verticalBias" },
        "Invalid vertical motor bias!" )[0];
}

int
main()
{
    using namespace el3D::utils;
    using camera_t = serviceData::camera_t<>;

    auto service =
        initializer::CreateService< camera_t, serviceData::cameraCommand_t >(
            SERVICE );
    LoadCustomConfig();

    auto cam =
        utils::elWebcam::Create( resolution[0], resolution[1], device[0] );
    std::function< void( camera_t* ) > getNextFrame;

    if ( cam.HasError() )
    {
        LOG_FATAL( 0 ) << "unable to initialize webcam on device " << device[0];
        exit( -1 );
    }

    if ( ( *cam )->StartStreaming().HasError() )
    {
        LOG_FATAL( 0 ) << "unable to stream with webcam " << device[0];
        exit( -1 );
    }

    driver::MotorController verticalMotor( verticalPin, verticalBias );
    driver::MotorController horizontalMotor( horizontalPin, horizontalBias );

    std::atomic_bool keepRunning{ true };
    std::thread      motorLoop( [&] {
        while ( keepRunning )
        {
            service.subscriber.take().and_then(
                [&]( iox::popo::Sample< const serviceData::cameraCommand_t >&
                         cmd ) {
                    LOG_INFO( 0 ) << "trying to setup new angle [ "
                                  << cmd->xAngle << ", " << cmd->yAngle << " ]";
                    verticalMotor.SetAngle( cmd->yAngle );
                    horizontalMotor.SetAngle( cmd->xAngle );
                } );
        }
    } );

    getNextFrame = [&]( camera_t* d ) {
        std::stringstream ss;
        auto              frame = ( *cam )->GetNextFrame( d->data );
        d->dataSize             = ( frame.HasError() ) ? 0u : *frame;
        d->width                = static_cast< uint16_t >( resolution[0] );
        d->height               = static_cast< uint16_t >( resolution[1] );
        d->angle.xAngle         = horizontalMotor.GetCurrentAngle();
        d->angle.yAngle         = verticalMotor.GetCurrentAngle();
        if ( frame.HasError() )
            LOG_WARN( 0 ) << "got invalid frame from webcam " << device[0];
    };

    for ( uint64_t transmissionID = 0; true; ++transmissionID )
    {
        service.publisher.publishResultOf( getNextFrame );
    }

    keepRunning.store( false );
    motorLoop.join();
}
