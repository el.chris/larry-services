#include "driver/RealSense.hpp"
#include "initializer/initializer.hpp"
#include "network/elEasyTCPMessaging_Server.hpp"
#include "network/etm/Transmission_t.hpp"
#include "network/etm/etm.hpp"
#include "serviceData/trackingCamera_t.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/systemInfoTypes.hpp"

#include <chrono>
#include <iostream>
#include <optional>
#include <ratio>

using namespace el3D;

constexpr char SERVICE[] = "TrackingCamera";

std::vector< std::string > trackingCameraName;
constexpr int              UPDATE_INTERVAL_MS = 100;


void
LoadCustomConfig()
{
    trackingCameraName = initializer::GetConfigEntry< std::string >(
        []( auto& v ) { return v.size() == 1; },
        { "services", SERVICE, "cameraName" },
        "Invalid tracking camera name!" );
}

int
main()
{
    auto service =
        initializer::CreateService< serviceData::trackingCamera_t >( SERVICE );

    LoadCustomConfig();

#ifdef ENABLE_LIBREALSENSE
    driver::RealSense realSense;
    auto              trackingCamera =
        realSense.AcquireTrackingCamera( trackingCameraName[0] );

    if ( !trackingCamera.has_value() )
    {
        LOG_FATAL( 0 ) << "device not found - terminating";
        exit( 1 );
    }

    trackingCamera->device.config.enable_stream( RS2_STREAM_POSE,
                                                 RS2_FORMAT_6DOF );
    trackingCamera->StartStreaming();

    auto getNextSample = [&]( serviceData::trackingCamera_t* sample ) {
        trackingCamera->GetOutput( *sample );
    };
    while ( true )
    {
        service.publisher.publishResultOf( getNextSample );
        std::this_thread::sleep_for(
            std::chrono::milliseconds( UPDATE_INTERVAL_MS ) );
    }

#else
    LOG_FATAL( 0 ) << "librealsense support disabled - terminating";
    exit( 1 );
#endif
}
