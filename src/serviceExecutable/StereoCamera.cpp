#include "driver/RealSense.hpp"
#include "initializer/initializer.hpp"
#include "network/elEasyTCPMessaging_Server.hpp"
#include "network/etm/Transmission_t.hpp"
#include "network/etm/etm.hpp"
#include "serviceData/stereoCamera_t.hpp"
#include "utils/byteStream_t.hpp"
#include "utils/systemInfoTypes.hpp"

#include <chrono>
#include <csignal>
#include <iostream>
#include <optional>

using namespace el3D;
using namespace serviceData;

// https://dev.intelrealsense.com/docs/post-processing-filters

constexpr char SERVICE[] = "StereoCamera";

std::vector< int >         framesPerSecond;
std::vector< int >         resolution;
std::vector< std::string > stereoCameraName;

std::atomic_bool keepRunning{ true };

void
LoadCustomConfig()
{
    resolution = initializer::GetConfigEntry< int >(
        []( auto &v ) { return v.size() == 2; },
        { "services", SERVICE, "resolution" },
        "Invalid stereo camera resolution!" );

    framesPerSecond = initializer::GetConfigEntry< int >(
        []( auto &v ) { return v.size() == 1; },
        { "services", SERVICE, "framesPerSecond" },
        "Invalid frames per second entry!" );

    stereoCameraName = initializer::GetConfigEntry< std::string >(
        []( auto &v ) { return v.size() == 1; },
        { "services", SERVICE, "cameraName" }, "Invalid stereo camera name!" );
}

void
Terminate( int )
{
    LOG_INFO( 0 ) << "terminating stereoCamera";
    keepRunning.store( false );
}

int
main()
{
    std::signal( SIGTERM, Terminate );
    std::signal( SIGINT, Terminate );

    auto service =
        initializer::CreateService< serviceData::stereoCamera_t >( SERVICE );

    LoadCustomConfig();

#ifdef ENABLE_LIBREALSENSE
    driver::RealSense realSense;
    auto stereoCamera = realSense.AcquireStereoCamera( stereoCameraName[0] );

    if ( !stereoCamera.has_value() )
    {
        LOG_FATAL( 0 ) << "device not found - terminating";
        exit( 1 );
    }

    stereoCamera->device.config.enable_stream( RS2_STREAM_DEPTH, resolution[0],
                                               resolution[1], RS2_FORMAT_Z16,
                                               framesPerSecond[0] );
    stereoCamera->device.config.enable_stream(
        RS2_STREAM_INFRARED, 1, resolution[0], resolution[1], RS2_FORMAT_Y8,
        framesPerSecond[0] );

    stereoCamera->StartStreaming();

    while ( keepRunning )
    {
        service.publisher.publishResultOf(
            [&]( serviceData::stereoCamera_t *sample ) {
                stereoCamera->GetOutput( *sample );
            } );
    }
#else
    LOG_FATAL( 0 ) << "librealsense support disabled - terminating";
    exit( 1 );
#endif
}

