#include "gpio/GPIOWriter.hpp"
#include "initializer/initializer.hpp"
#include "network/etm/Transmission_t.hpp"
#include "serviceData/ledControl_t.hpp"

#include <array>

using namespace el3D;

constexpr char SERVICE[]  = "LedControl";
constexpr int  RGB_PINS[] = { 3, 2, 5 };

class LedControl
{
  public:
    LedControl( const std::array< int, 3 >& rgbPins ) noexcept;

    LedControl&
    Red( const bool ) noexcept;
    LedControl&
    Green( const bool ) noexcept;
    LedControl&
    Blue( const bool ) noexcept;

  private:
    gpio::GPIOWriter red;
    gpio::GPIOWriter green;
    gpio::GPIOWriter blue;
};

LedControl::LedControl( const std::array< int, 3 >& rgbPins ) noexcept
    : red( rgbPins[0] ), green( rgbPins[1] ), blue( rgbPins[2] )
{
}

LedControl&
LedControl::Red( const bool v ) noexcept
{
    ( v ) ? red.High() : red.Low();
    return *this;
}

LedControl&
LedControl::Green( const bool v ) noexcept
{
    ( v ) ? green.High() : green.Low();
    return *this;
}

LedControl&
LedControl::Blue( const bool v ) noexcept
{
    ( v ) ? blue.High() : blue.Low();
    return *this;
}

int
main()
{
    auto service =
        initializer::CreateService< serviceData::ledControl_t >( SERVICE );
    service.publisher.publishCopyOf( serviceData::ledControl_t() );

    LedControl ledFront( { RGB_PINS[0], RGB_PINS[1], RGB_PINS[2] } );

    while ( true )
    {
        service.subscriber.take().and_then(
            [&]( iox::popo::Sample< const serviceData::ledControl_t >&
                     ledState ) {
                ledFront.Red( ledState->red )
                    .Green( ledState->green )
                    .Blue( ledState->blue );
                LOG_INFO( 0 ) << "led command received: RGB = "
                              << static_cast< int64_t >( ledState->red )
                              << static_cast< int64_t >( ledState->green )
                              << static_cast< int64_t >( ledState->blue );
                service.publisher.publishCopyOf( *ledState.get() );
            } );
    }
}
