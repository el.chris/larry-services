#include "driver/MotorController.hpp"
#include "gpio/GPIOReader.hpp"
#include "gpio/GPIOWriter.hpp"
#include "iceoryx_hoofs/cxx/string.hpp"
#include "initializer/initializer.hpp"
#include "serviceData/ultraSonicSensor_t.hpp"
#include "units/Time.hpp"
#include "units/Velocity.hpp"

#include <chrono>
#include <limits>
#include <sys/time.h>

using namespace el3D;

constexpr char SERVICE[] = "UltraSonicSensor";

class UltraSonicSensor
{
  public:
    UltraSonicSensor( const int echoPin, const int triggerPin,
                      const int motorPin, const int motorAngleBias ) noexcept;

    ~UltraSonicSensor();

    void
    ActivateSensorSweep( const bool doActivate ) noexcept;
    serviceData::ultraSonicSensor_t
    GetSensorData() const noexcept;
    void
    SetAngle( const uint16_t angle ) noexcept;

  private:
    void
    MotorLoop() noexcept;
    bb::float_t< 64 >
    CalculateDistance() noexcept;

  private:
    using number_t                   = bb::float_t< 64 >;
    static constexpr number_t NODATA = std::numeric_limits< number_t >::max();

    gpio::GPIOReader        echo;
    gpio::GPIOWriter        trigger;
    int                     motorAngleBias;
    driver::MotorController motor;

    std::atomic_bool                keepRunning{ true };
    std::atomic_bool                hasActiveSensorSweep{ false };
    serviceData::ultraSonicSensor_t currentState;
    mutable std::mutex              currentStateMutex;
    std::thread                     motorLoopThread;
};

UltraSonicSensor::UltraSonicSensor( const int echoPin, const int triggerPin,
                                    const int motorPin,
                                    const int motorAngleBias ) noexcept
    : echo( echoPin ), trigger( triggerPin ), motorAngleBias( motorAngleBias ),
      motor( motorPin, motorAngleBias )
{
    this->motorLoopThread = std::thread( [this] { this->MotorLoop(); } );
}

UltraSonicSensor::~UltraSonicSensor()
{
    this->keepRunning = false;
    this->motorLoopThread.join();
}

void
UltraSonicSensor::ActivateSensorSweep( const bool doActivate ) noexcept
{
    if ( doActivate == this->hasActiveSensorSweep.load() ) return;

    this->hasActiveSensorSweep.store( doActivate );
    this->motor.SetAngle( 90 );
}

void
UltraSonicSensor::SetAngle( const uint16_t angle ) noexcept
{
    this->motor.SetAngle( angle );
}

void
UltraSonicSensor::MotorLoop() noexcept
{
    bool moveUp{ true };
    while ( this->keepRunning )
    {

        auto currentAngle = this->motor.GetCurrentAngle();
        auto distance     = this->CalculateDistance();

        {
            std::lock_guard< std::mutex > lock( this->currentStateMutex );
            this->currentState.angle    = units::Angle::Degree( currentAngle );
            this->currentState.distance = units::Length::Meter( distance );
        }

        if ( this->hasActiveSensorSweep.load() )
        {
            if ( currentAngle >= 180 + this->motorAngleBias )
                moveUp = false;
            else if ( currentAngle <= 0 - this->motorAngleBias )
                moveUp = true;

            ( moveUp ) ? this->motor.Up() : this->motor.Down();
        }
    }
}

serviceData::ultraSonicSensor_t
UltraSonicSensor::GetSensorData() const noexcept
{
    std::lock_guard< std::mutex > lock( this->currentStateMutex );
    return this->currentState;
}

bb::float_t< 64 >
UltraSonicSensor::CalculateDistance() noexcept
{
    units::Time     outOfReach    = units::Time::MilliSeconds( 30 );
    units::Time     burstDuration = units::Time::MicroSeconds( 20.0 );
    units::Velocity speedOfSound  = units::Velocity::MeterPerSecond( 343.2 );

    this->trigger.Low( units::Time::MicroSeconds( 2.0 ) );
    this->trigger.High( burstDuration );
    this->trigger.Low();

    auto startOfMeasurement = std::chrono::high_resolution_clock::now();
    auto stop               = startOfMeasurement;

    while ( !this->echo.IsHigh() )
    {
        stop = std::chrono::high_resolution_clock::now();

        if ( std::chrono::duration_cast< std::chrono::microseconds >(
                 stop - startOfMeasurement )
                 .count() >
             static_cast< long int >( outOfReach.GetMicroSeconds() ) )
        {
            return NODATA;
        }
        std::this_thread::sleep_for( std::chrono::microseconds( 1 ) );
    }

    auto startOfEcho = std::chrono::high_resolution_clock::now();
    while ( this->echo.IsHigh() )
    {
        stop = std::chrono::high_resolution_clock::now();

        if ( std::chrono::duration_cast< std::chrono::microseconds >(
                 stop - startOfEcho )
                 .count() >
             static_cast< long int >( outOfReach.GetMicroSeconds() ) )
        {
            return NODATA;
        }
        std::this_thread::sleep_for( std::chrono::microseconds( 1 ) );
    }

    stop = std::chrono::high_resolution_clock::now();

    return static_cast< double >(
               std::chrono::duration_cast< std::chrono::nanoseconds >(
                   stop - startOfMeasurement )
                   .count() ) /
           1000000000.0 * speedOfSound.GetMeterPerSecond() / 2.0;
}

int echoPin;
int triggerPin;
int motorPin;
int motorBias;

void
LoadCustomConfig()
{
    echoPin = initializer::GetConfigEntry< int >(
        []( auto& ) { return true; }, { "services", SERVICE, "echoPin" },
        "" )[0];
    triggerPin = initializer::GetConfigEntry< int >(
        []( auto& ) { return true; }, { "services", SERVICE, "triggerPin" },
        "" )[0];
    motorPin = initializer::GetConfigEntry< int >(
        []( auto& ) { return true; }, { "services", SERVICE, "motorPin" },
        "" )[0];
    motorBias = initializer::GetConfigEntry< int >(
        []( auto& ) { return true; }, { "services", SERVICE, "motorBias" },
        "" )[0];
}

int
main()
{
    auto service =
        initializer::CreateService< serviceData::ultraSonicSensor_t,
                                    serviceData::ultraSonicSensorCommand_t >(
            SERVICE );

    UltraSonicSensor ultraSonicSensor( echoPin, triggerPin, motorPin,
                                       motorBias );

    while ( true )
    {
        service.subscriber.take().and_then(
            [&]( iox::popo::Sample<
                 const serviceData::ultraSonicSensorCommand_t >& command )
            {
                LOG_INFO( 0 )
                    << "received command [sensorsweep = " << std::boolalpha
                    << command->doPerformSensorSweep
                    << ", reset angle = " << std::dec << command->setAngle
                    << "]";

                ultraSonicSensor.SetAngle( command->setAngle );
                ultraSonicSensor.ActivateSensorSweep(
                    command->doPerformSensorSweep );
            } );

        service.publisher.publishCopyOf( ultraSonicSensor.GetSensorData() );
        std::this_thread::sleep_for( std::chrono::milliseconds( 25 ) );
    }
}
