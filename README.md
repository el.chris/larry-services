# larry-services

If you encounter problems please take a look into the [FAQ](https://gitlab.com/larry.robotics/larry.robotics/blob/master/FAQ.md).
Before compiling larry-services make sure you have installed librealsense2 and
cmake >= 3.13. If you would like to use them only for testing purposes you can install 
them without GPIO and librealsense support.

## (optional) Install librealsense2 for Stereo and Tracking Camera Support

1. Clone repository
```sh
git clone https://github.com/IntelRealSense/librealsense
```

2. Setup device permissions.
```sh
cd librealsense
sudo ./scripts/setup_udev_rules.sh
```

3. Correct wrong compile flags in librealsense. Open file ```CMake/unix_config.cmake``` and update lines to the following extract. This needs to be done only when you are compiling on the Raspberry Pi 4 and are using
the ArchLinux distribution.
```
# if(${MACHINE} MATCHES "arm-linux-gnueabihf")
#     set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -mfpu=neon -mfloat-abi=hard -ftree-vectorize -latomic")
#     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mfpu=neon -mfloat-abi=hard -ftree-vectorize -latomic")
# elseif(${MACHINE} MATCHES "aarch64-linux-gnu")

    set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -march=native -O2 -ftree-vectorize")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -O2 -ftree-vectorize")

# else()
#     set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -mssse3")
#     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mssse3")
#     set(LRS_TRY_USE_AVX true)
# endif(${MACHINE} MATCHES "arm-linux-gnueabihf")
```

3. Compile and install librealsense. This will take a while so grab a coffee!
```sh
cmake -Bbuild -H. -DBUILD_EXAMPLES=false -DCMAKE_BUILD_TYPE=Release -DFORCE_RSUSB_BACKEND=true
cd build
make -j4
sudo make install
sudo ln -s /usr/local/lib/lib* /usr/lib/
```

## Compile larry-services

If you would like to use them on Larry you have to activate ```-DGPIO_SUPPORT=ON``` but
when you would like to run them for testing purposes on your Laptop for instance you need to
deactivate them! Activated GPIO support requires an installed ```wiringPi``` library 
(see https://github.com/WiringPi/WiringPi, Tag: `final_source_2.50`).

### On Larry Robot with activated GPIO and LIBREALSENSE support
```sh
cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Release -DGPIO_SUPPORT=ON
cd build
make -j4
```

### Development for Local Deployment
```sh
cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Release -DGPIO_SUPPORT=OFF -DLIBREALSENSE_SUPPORT=OFF
cd build
make -j4
```

## Starting larry-services

All services can be started at once by starting them with our execution manager `./build/services/ServiceStarter`. 
If you would like to start a single service change into the root folder of the repository and call `./build/services/Camera`
for instance to start the camera service. But make sure that our central daemon RouDi is running (`./build/RouDi`).

## Configuring larry-services

The configuration file can be found in ```config/services.lua```.

 * **services** - Main entry where all service related configuration is stored.
    * **active** - Lists all active services which should be monitored by the ServiceInfoProvider.
    * **ServiceName** - Can be for instance **Camera**, **Drive** or something else. Under this entry
                            all the service specific entries are stored.
        * **version** - ETM (Easy TCP Messaging) Protocol Service Version, can be set to any integer. If you
                            would like to connect to services they have to have the same version.
        * **logfile** - Name of the logfile under which all service related messages should be logged.
        * **port** - TCP port under which the service should listen.
        * All other entries are service specific configurations.

When you are configuring the camera services you can use the command
```sh
v4l2-ctl --list-devices
```
to look up the corresponding camera device.
