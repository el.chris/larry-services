Robot = {
    name    = "larry",
    domain  = "larry_robotics",
}

IceOryx = {
    eventCommand        = "command",
    eventInfo           = "info",
    eventSimulation     = "simulation",
    eventAnnounce       = "announce",
}

management = {
    active = {
        "RouDi",
        --"Iceoryx_ETM_Gateway"
    },

    RouDi = {
        path = "./build/iox-roudi",
    },
    Iceoryx_ETM_Gateway = {
        path = "./build/Iceoryx_ETM_Gateway",
    },
}

services = {
    logDirectory = "log",
    active = {
      "Identifier",
      "TrackingCamera",
      "StereoCamera",
      "Drive",
      "LedControl",
      "SystemMonitor",
      "TrackingSensor",
      "UltraSonicSensor",
      "Camera",
    },
    Identifier = {
        version     = 0,
        command     = "services/Identifier",
        logfile     = "Identifier.log",
        port        = 5545,
    },
    Camera = {
        version        = 0,
        command        = "services/Camera",
        logfile        = "Camera.log",
        port           = 5563,
        resolution     = {640, 480},
        device         = "/dev/video76",
        horizontalPin  = 14,
        horizontalBias = 0,
        verticalPin    = 13,
        verticalBias   = 0,
    },
    Drive = {
        version     = 0,
        command     = "services/Drive",
        logfile     = "Drive.log",
        port        = 5570,
    },
    StereoCamera = {
        version             = 0,
        command             = "services/StereoCamera",
        logfile             = "StereoCamera.log",
        port                = 5569,
        resolution          = {640, 480},
        framesPerSecond     = 30,
        cameraName          = "Intel RealSense D435I",
    },
    TrackingCamera = {
        version             = 0,
        command             = "services/TrackingCamera",
        logfile             = "TrackingCamera.log",
        port                = 5583,
        cameraName          = "Intel RealSense T265",
    },
    SystemMonitor = {
        version     = 0,
        command     = "services/SystemMonitor",
        logfile     = "SystemMonitor.log",
        port        = 5567,
    },
    LedControl = {
        version     = 0,
        command     = "services/LedControl",
        logfile     = "LedControl.log",
        port        = 5571,
    },
    TrackingSensor = {
        version     = 0,
        command     = "services/TrackingSensor",
        logfile     = "TrackingSensor.log",
        port        = 5572,
    },
    UltraSonicSensor = {
        version     = 0,
        command     = "services/UltraSonicSensor",
        logfile     = "UltraSonicSensor.log",
        port        = 5564,
        motorBias   = -16,
        echoPin     = 30,
        triggerPin  = 31,
        motorPin    = 4,
    },
    UltraSonicSensorCommand = {
        version     = 0,
        command     = "services/MotorControl UltraSonicSensorCommand",
        logfile     = "UltraSonicSensor.log",
        port        = 5584,
    }, 
}
