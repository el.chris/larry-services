cmake_minimum_required( VERSION 3.13 )

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(FetchContent)

option(USE_SANITIZER "use address and undefined behavior sanitizer" OFF)
option(USE_TCMALLOC  "use tcmalloc from the google performance tools" OFF)
option(BUILD_UNITTESTS "enable it if you'd like to compile all unittests" OFF)
option(USE_CCACHE "use ccache to increase compile time during development" OFF)
option(EXTENDED_CODE_ANALYSIS "activates clang-tidy for a more extensive code analysis" OFF)
option(GPIO_SUPPORT "requires wiringPi lib installed to enable gpio support" OFF)
option(SERVICE_LIBRARY_ONLY "compiles only the libraries required to work with larry-services" OFF)
option(LIBREALSENSE_SUPPORT "enables services which need librealsense2 to be installed" ON)
option(USE_CUSTOM_REPO "activate interal custom repo - only for development" ON)

if ( USE_CUSTOM_REPO )
    set(REPO_SRC "ssh://gitea@elchris.org:5522/elchris/")
else()
    set(REPO_SRC "https://gitlab.com/el.chris/")
endif(USE_CUSTOM_REPO )

if ( NOT TARGET RouDi )
    FetchContent_Declare(
        ICEORYX
        GIT_REPOSITORY https://github.com/eclipse/iceoryx.git
        #GIT_TAG 0a81dde3fe5bd8c4738c9149a818574fdeb51282
    )
    FetchContent_GetProperties(IceOryx)
    if (NOT iceoryx_POPULATED)
        message(STATUS "updating: Ice0ryx" )
        FetchContent_Populate(ICEORYX)

        add_subdirectory(${iceoryx_SOURCE_DIR}/iceoryx_hoofs ${iceoryx_BINARY_DIR}/iceoryx_hoofs)
        add_subdirectory(${iceoryx_SOURCE_DIR}/iceoryx_posh  ${iceoryx_BINARY_DIR}/iceoryx_posh)
    endif()
endif()

if ( NOT TARGET cuipron )
    FetchContent_Declare(
        CUIPRON
        GIT_REPOSITORY ${REPO_SRC}cuipron.git
    )
    FetchContent_GetProperties(cuipron)
    if(NOT cuipron_POPULATED)
        message(STATUS "updating: cuipron [${REPO_SRC}]")
        FetchContent_Populate(CUIPRON)

        add_subdirectory(${cuipron_SOURCE_DIR} ${cuipron_BINARY_DIR})
    endif()
endif()

if ( NOT TARGET 3Delch)
    FetchContent_Declare(
        3DELCH
        GIT_REPOSITORY ${REPO_SRC}3delch.git
    )
    FetchContent_GetProperties(3Delch)
    if (NOT 3delch_POPULATED)
        message(STATUS "updating: 3Delch [${REPO_SRC}]" )
        FetchContent_Populate(3DELCH)

        set(ONLY_UTILS ON)
        set(ADD_LUA_BINDINGS OFF)

        add_subdirectory(${3delch_SOURCE_DIR} ${3delch_BINARY_DIR})
    endif()
endif()

message("")
message("######################")
message("### larry-services ###")
message("######################")
message("")

set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/services)

set(ADD_LUA_BINDINGS ON)

project( larry-services )
set_property ( GLOBAL PROPERTY USE_FOLDERS ON )
set( CMAKE_EXPORT_COMPILE_COMMANDS ON )

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Debug")
endif (NOT CMAKE_BUILD_TYPE)

if ( WIN32 )
    set( CMAKE_BUILD_TYPE "Release")
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /GR /MT /std:c++latest" )
    set( CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT /DRELEASE /std:c++latest")
    set( CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MT /std:c++latest")
    set( CMAKE_BUILD_PARALLEL_LEVEL 12 )
    set( EXTERNALPROJECT_CXX_FLAGS /MD /O2 )
    set( USE_SYSTEM_LIBS ON)
else()
    set(C_FLAGS                     "")
    set(CXX_FLAGS                   "${CMAKE_CXX_FLAGS} -pipe")
    
    set(CXX_FLAGS_STD               "-std=c++2a")
    set(CXX_FLAGS_DEBUG             "-Og -g3")
    set(CXX_FLAGS_RELEASE           "-DRELEASE -O3 -march=native")
    set(CXX_FLAGS_WARNINGS          "-W -Wall -pedantic -Wextra -Wconversion -Wuninitialized -Wno-abi -Wno-c++20-designator")
    set(CXX_FLAGS_DISABLE_WARNINGS  "-w")

    if (CMAKE_BUILD_TYPE MATCHES "Debug")
        set( C_FLAGS "${CXX_FLAGS} ${CXX_FLAGS_DEBUG}" )
        set( CXX_FLAGS "${CXX_FLAGS} ${CXX_FLAGS_STD} ${CXX_FLAGS_DEBUG}" )
        set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_FLAGS} ${CXX_FLAGS_WARNINGS}" )
    elseif (CMAKE_BUILD_TYPE MATCHES "Release")
        set( C_FLAGS "${CXX_FLAGS} ${CXX_FLAGS_RELEASE}" )
        set( CXX_FLAGS "${CXX_FLAGS} ${CXX_FLAGS_STD} ${CXX_FLAGS_RELEASE}" )
        set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_FLAGS}" )
    endif (CMAKE_BUILD_TYPE MATCHES "Debug")

    if ( USE_SANITIZER ) 
        set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fsanitize=undefined" )
    endif(USE_SANITIZER)

    if ( USE_TCMALLOC ) 
        set( CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -ltcmalloc" )
    endif(USE_TCMALLOC)
endif( WIN32 )

include_directories(
    "${CMAKE_CURRENT_LIST_DIR}/src/"
        "${CMAKE_CURRENT_LIST_DIR}/inline/"
        "${CMAKE_CURRENT_LIST_DIR}/include/"
        )

find_package(Threads)

include(ProcessorCount)
ProcessorCount(PROCESSOR_COUNT)

if ( EXTENDED_CODE_ANALYSIS )
    find_program(
        CLANG_TIDY_EXE
        NAMES "clang-tidy"
    )
endif( EXTENDED_CODE_ANALYSIS )

if(CLANG_TIDY_EXE)
    set(PERFORM_CLANG_TIDY "${CLANG_TIDY_EXE}" "-checks=*,-clang-analyzer-alpha.*")
endif(CLANG_TIDY_EXE)

if (LIBREALSENSE_SUPPORT) 
    add_compile_definitions(ENABLE_LIBREALSENSE)
    set(LIBREALSENSE_LINKER realsense2)
else()
    set(LIBREALSENSE_LINKER "")
endif (LIBREALSENSE_SUPPORT) 

if (GPIO_SUPPORT)
    add_compile_definitions(ENABLE_GPIO)
    find_path(WIRINGPI_PATH
        NAMES wiringPi.h 
        PATHS 
            /usr/include
            /usr/local/include
            /usr/include/wiringpi
            /usr/local/include/wiringpi
            )
    if ( NOT WIRINGPI_PATH )
        message(FATAL_ERROR "wiringPi is required in GPIO mode")
    endif( NOT WIRINGPI_PATH )

    find_library(wiringPi_LIB wiringPi)
    include_directories(${WIRINGPI_PATH})
    set(ADDITIONAL_LIBRARIES ${wiringPi_LIB} ncurses)
endif(GPIO_SUPPORT)

if (CMAKE_BUILD_TYPE MATCHES "Debug")
    add_compile_definitions(ELCH_DEBUG)
else ()
    remove_definitions(-DELCH_DEBUG)
endif (CMAKE_BUILD_TYPE MATCHES "Debug")

file ( GLOB_RECURSE DRIVER_SRC
    "${CMAKE_CURRENT_LIST_DIR}/src/driver/*.cpp"
    )

file ( GLOB_RECURSE SERVICE_DATA_SRC
    "${CMAKE_CURRENT_LIST_DIR}/src/serviceData/*.cpp"
    )

file ( GLOB_RECURSE INITIALIZER_SRC
    "${CMAKE_CURRENT_LIST_DIR}/src/initializer/*.cpp"
    )

file ( GLOB_RECURSE GPIO_SRC
    "${CMAKE_CURRENT_LIST_DIR}/src/gpio/*.cpp"
    )

message("       Libraries")
add_library( gpio ${GPIO_SRC} )
target_link_libraries( gpio ${CMAKE_THREAD_LIBS_INIT} units )
message("           gpio")

add_library( serviceData ${SERVICE_DATA_SRC} )
target_link_libraries( serviceData ${CMAKE_THREAD_LIBS_INIT} utils iceoryx_posh )
target_include_directories( serviceData 
  PUBLIC
    ${CMAKE_CURRENT_LIST_DIR}/include 
    ${CMAKE_CURRENT_LIST_DIR}/inline
)
message("           serviceData")

add_library( driver ${DRIVER_SRC} )
target_link_libraries( driver ${CMAKE_THREAD_LIBS_INIT} utils gpio serviceData ${LIBREALSENSE_LINKER} )
target_include_directories( driver 
  PUBLIC
    ${CMAKE_CURRENT_LIST_DIR}/include 
    ${CMAKE_CURRENT_LIST_DIR}/inline
)
message("           driver")

add_library( initializer ${INITIALIZER_SRC} )
target_link_libraries( initializer ${CMAKE_THREAD_LIBS_INIT} utils lua ${LUA_LIBRARIES} network iceoryx_posh )
message("           initializer")

if ( NOT SERVICE_LIBRARY_ONLY )
    message("")
    message("       Services")
    add_executable( Camera "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/Camera.cpp" )
    target_link_libraries( Camera ${CMAKE_THREAD_LIBS_INIT} serviceData initializer driver )
    message("           Camera")

    add_executable( Drive "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/Drive.cpp" )
    target_link_libraries( Drive ${CMAKE_THREAD_LIBS_INIT} serviceData initializer gpio ${ADDITIONAL_LIBRARIES} )
    message("           Drive")

    add_executable( Identifier "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/Identifier.cpp" )
    target_link_libraries( Identifier ${CMAKE_THREAD_LIBS_INIT} serviceData initializer gpio ${ADDITIONAL_LIBRARIES} )
    message("           Identifier")

    add_executable( LedControl "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/LedControl.cpp" )
    target_link_libraries( LedControl ${CMAKE_THREAD_LIBS_INIT} serviceData initializer gpio ${ADDITIONAL_LIBRARIES} )
    message("           LedControl")

    add_executable( StereoCamera "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/StereoCamera.cpp" )
    target_link_libraries( StereoCamera ${CMAKE_THREAD_LIBS_INIT} serviceData initializer ${LIBREALSENSE_LINKER} driver )
    message("           StereoCamera")

    add_executable( TrackingCamera "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/TrackingCamera.cpp" )
    target_link_libraries( TrackingCamera ${CMAKE_THREAD_LIBS_INIT} serviceData initializer ${LIBREALSENSE_LINKER} driver )
    message("           TrackingCamera")

    add_executable( SystemMonitor "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/SystemMonitor.cpp" )
    target_link_libraries( SystemMonitor ${CMAKE_THREAD_LIBS_INIT} initializer )
    message("           SystemMonitor")

    add_executable( TrackingSensor "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/TrackingSensor.cpp" )
    target_link_libraries( TrackingSensor ${CMAKE_THREAD_LIBS_INIT} serviceData initializer gpio ${ADDITIONAL_LIBRARIES} )
    message("           TrackingSensor")

    add_executable( UltraSonicSensor "${CMAKE_CURRENT_LIST_DIR}/src/serviceExecutable/UltraSonicSensor.cpp" )
    target_link_libraries( UltraSonicSensor ${CMAKE_THREAD_LIBS_INIT} serviceData initializer gpio driver ${ADDITIONAL_LIBRARIES} )
    message("           UltraSonicSensor")

    message("")
    message("       Apps")
    add_executable( Explorer "${CMAKE_CURRENT_LIST_DIR}/src/appExecutable/Explorer.cpp" )
    target_link_libraries( Explorer ${CMAKE_THREAD_LIBS_INIT} serviceData initializer gpio ${ADDITIONAL_LIBRARIES} )
    set_target_properties( Explorer PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/apps" )
    message("           Explorer")
    add_executable( ServiceStarter "${CMAKE_CURRENT_LIST_DIR}/src/appExecutable/ServiceStarter.cpp" )
    target_link_libraries( ServiceStarter ${CMAKE_THREAD_LIBS_INIT} cui serviceData initializer gpio ${ADDITIONAL_LIBRARIES} )
    message("           ServiceStarter")
    add_executable( Tester "${CMAKE_CURRENT_LIST_DIR}/src/appExecutable/Tester.cpp" )
    target_link_libraries( Tester ${CMAKE_THREAD_LIBS_INIT} cui serviceData initializer gpio ${ADDITIONAL_LIBRARIES} )
    set_target_properties( Tester PROPERTIES RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/apps" )
    message("           Tester")
endif()


message("")
message("")
message("       Build Properties")
message("")
message("           type........................: " ${CMAKE_BUILD_TYPE})
message("           project name................: " ${CMAKE_PROJECT_NAME})
message("           number of cpus..............: " ${PROCESSOR_COUNT})
message("           c compiler..................: " ${CMAKE_C_COMPILER})
message("           c++ compiler................: " ${CMAKE_CXX_COMPILER})
message("           c++ flags...................: " ${CMAKE_CXX_FLAGS})
message("           linker flags................: " ${CMAKE_EXE_LINKER_FLAGS})
if ( UNIX )
message("           ccache found................: " ${CCACHE_FOUND})
endif ( UNIX )
message("           clang-tidy..................: " ${CLANG_TIDY_EXE})
message("")
message("       Options")
message("           GPIO_SUPPORT................: " ${GPIO_SUPPORT})
message("           SERVICE_LIBRARY_ONLY........: " ${SERVICE_LIBRARY_ONLY})
message("           EXTENDED_CODE_ANALYSIS......: " ${EXTENDED_CODE_ANALYSIS})
message("           USE_CCACHE..................: " ${USE_CCACHE})
message("           USE_SANITIZER...............: " ${USE_SANITIZER})
message("           USE_SYSTEM_LIBS.............: " ${USE_SYSTEM_LIBS})
message("           USE_TCMALLOC................: " ${USE_TCMALLOC})
message("           LIBREALSENSE_SUPPORT........: " ${LIBREALSENSE_SUPPORT})
message("           USE_CUSTOM_REPO.............: " ${USE_CUSTOM_REPO})
message("")
if (CMAKE_BUILD_TYPE MATCHES "Debug")
message("       hint: if you want to build in the release mode simply add -DCMAKE_BUILD_TYPE=Release")
endif (CMAKE_BUILD_TYPE MATCHES "Debug")
message("")
